module.exports = function()
{
	"use strict";
	var remover = require('rimraf');
	var child_process = require('child_process');

	var errorLogger = function(err)
	{
		if(err)
		{
			console.log(err);
		}
	};

	var installProcess = child_process.exec('npm install', 
		function(err,out,stderr){});

	var pruneProcess = child_process.exec('npm prune', 
		function(err,out,stderr){});

	remover('./lib', errorLogger);
	remover('./resources', errorLogger);
	remover('./setup_project.js', errorLogger);	
}
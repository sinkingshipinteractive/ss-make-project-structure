/* global module, console, require */



module.exports = function() {
	"use strict";

	var fs = require('fs-extra');

	function makeFolderSync (path) {
		try {
			fs.mkdirSync(path);
		} catch (e) {
			if (e.code != "EEXIST") {
				throw e;
				console.log("BOO!");
			}
		}
	}

	console.log("Copying resources for project type: " + global.project_type);

	if(global.project_type == "c") {
		fs.copySync("./resources/gulpfile.js", './gulpfile.js');
	} else if (global.project_type == "p") {
		fs.copySync("./resources/gulpfile-pixi.js", './gulpfile.js');
	}

	fs.copySync("./resources/package.json", './package.json');
	fs.copySync("./resources/bower.json", './bower.json');
	fs.copySync("./resources/.bowerrc", './.bowerrc');

	if(global.project_type == "c") {
		fs.copySync("./resources/index.html", './src/index.html');
	} else if (project_type == "p") {
		fs.copySync("./resources/index_pixi.html", './src/index.html');
	}
	fs.copySync("./resources/index_compiled.html", './src/index_compiled.html');

	if(global.project_type == "c") {
		fs.copySync("./resources/main.js", './src/js/main.js');
	} else if (project_type == "p") {
		fs.copySync("./resources/main_pixi.js", './src/js/main.js');
	}


	if(global.project_type == "p") {
		console.log("copying pixi specific resources.")
		makeFolderSync("./src/config");
		makeFolderSync("./src/config/assets");
		fs.copySync("./resources/pixi_specific/config/global.json", "./src/config/global.json");
		fs.copySync("./resources/pixi_specific/config/assets/globalAssets.json", "./src/config/assets/globalAssets.json");
		fs.copySync("./resources/pixi_specific/config/assets/preloadAssets.json", "./src/config/assets/preloadAssets.json");

		makeFolderSync("./src/images/preloader");
		fs.copySync("./resources/pixi_specific/images/preloader/PlayButton.png", "./src/images/preloader/PlayButton.png");

		makeFolderSync("./src/js/managers");
		fs.copySync("./resources/pixi_specific/js/managers/GameManager.js", "./src/js/managers/GameManager.js");

		makeFolderSync("./src/js/scenes");
		fs.copySync("./resources/pixi_specific/js/scenes/PreloadScene.js", "./src/js/scenes/PreloadScene.js");
	}


};


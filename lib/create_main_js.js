/* global module, require */
module.exports = function() {
	"use strict";
	var fs = require("fs");


	fs.exists("./src/js/main.js", function(exists) {
		if (!exists) {
			var mainContents = "/*global require, ss, $, console*/";
			makeFolderSync("./src/js");
			fs.writeFileSync("./src/js/main.js", mainContents);
		}
	});

	function makeFolderSync (path) {
		try {
			fs.mkdirSync(path);
		} catch (e) {
			if (e.code != "EEXIST") {
				throw e;
			}
		}
	}
};

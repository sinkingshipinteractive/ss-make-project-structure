/* global module, require, console */

module.exports = function() {
	"use strict";

	var fs = require("fs");

	console.log("Creating project folders if they don't already exist.");

	makeFolderSync("./builds");
	makeFolderSync("./builds/release");
	makeFolderSync("./builds/release/audio");
	makeEmptyFolderText("./builds/release/audio");
	makeFolderSync("./builds/release/fonts");
	makeEmptyFolderText("./builds/release/fonts");
	makeFolderSync("./builds/release/videos");
	makeEmptyFolderText("./builds/release/videos");
	makeFolderSync("./builds/release/data");
	makeEmptyFolderText("./builds/release/data");
	makeFolderSync("./builds/release/images");
	makeEmptyFolderText("./builds/release/images");

	makeFolderSync("./src");
	makeFolderSync("./src/css");
	makeEmptyFolderText("./src/css");
	makeFolderSync("./src/images");
	makeEmptyFolderText("./src/images");
	makeFolderSync("./src/data");
	makeEmptyFolderText("./src/data");
	makeFolderSync("./src/js");
	makeEmptyFolderText("./src/js");
	makeFolderSync("./src/audio");
	makeEmptyFolderText("./src/audio");
	makeFolderSync("./src/videos");
	makeEmptyFolderText("./src/videos");
	if(global.project_type == "c") {
		makeFolderSync("./src/js/config");
		makeEmptyFolderText("./src/js/config");
		makeFolderSync("./src/js/config/bezier");
		makeEmptyFolderText("./src/js/config/bezier");
	}
	makeFolderSync("./src/fonts");
	makeEmptyFolderText("./src/fonts");
	makeFolderSync("./RawAssets");
	makeEmptyFolderText("./RawAssets");
	makeFolderSync("./RawAssets/svg");
	makeEmptyFolderText("./RawAssets/svg");
	makeFolderSync("./ref");
	makeFolderSync("./ref/Documents");
	makeEmptyFolderText("./ref/Documents");
	makeFolderSync("./ref/Designs");
	makeEmptyFolderText("./ref/Designs");

	console.log("Done creating project folders.");

	function makeFolderSync (path) {
		try {
			fs.mkdirSync(path);
		} catch (e) {
			if (e.code != "EEXIST") {
				throw e;
				console.log("BOO!");
			}
		}
	}

	function makeEmptyFolderText(path)
	{
		fs.exists(path+"/.emptyfolder", function(exists) {
			if (!exists) {
				var mainContents = "we in there but best delete me son!";
				makeFolderSync(path);
				fs.writeFileSync(path+"/.emptyfolder", mainContents);
			}
		});
	}
};

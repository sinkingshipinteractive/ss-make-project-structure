/* global module, require */
module.exports = function() {
	"use strict";
	var fs = require("fs");

	fs.exists("README.md", function(exists) {
		if (!exists) {
			var mainContents = "";
			fs.writeFileSync("README.md", mainContents);
		}
	});

};

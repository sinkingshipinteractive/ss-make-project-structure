global.project_type = "c";

module.exports = function()
{
	var readline = require("readline");
	var fs = require('fs-extra');
	var child_process = require('child_process');
	var remover = require('rimraf');


	//######## SUBMODULES ###########	

	var retry = function()
	{
		if(project_type == "c") {
			fs.exists('./src/js/weblib', function(exists)
			{
				if(!exists)
				{
					tryLogin();
				}
			});
		} else if (project_type == "p") {
			fs.exists('./src/js/weblib-pixi', function(exists)
			{
				if(!exists)
				{
					tryLogin();
				}
			});
		}
	};

	var tryLogin = function()
	{
		if(project_type == "c") {
			var child = child_process.spawn('git', 
			['submodule', 'add', 'https://bitbucket.org/sinkingshipinteractive/weblib', './src/js/weblib'],{
			stdio: [
				0,
				0,
				0]
			});

			child.addListener("exit", retry);
		} else if (project_type == "p") {
			var child = child_process.spawn('git', 
			['submodule', 'add', 'https://bitbucket.org/sinkingshipinteractive/weblib-pixi', './src/js/weblib-pixi'],{
			stdio: [
				0,
				0,
				0]
			});

			child.addListener("exit", retry);

			var child2 = child_process.spawn('git',
			['submodule', 'add', 'https://github.com/pixijs/pixi-spine', './src/js/pixi-spine'],{
			stdio: [
				0,
				0,
				0]
			});

		}
	};

	var startSubModules = function()
	{
		var initProcess = child_process.spawn('git', ['init'], {stdio: [0,0,0]});
		tryLogin();
	}

	// ########## BOWER PACKAGES ################
	
	var errorLogger = function(err)
	{
		if(err)
		{
			console.log(err);
		}
	};

	var copyCurrentBower = function(err, out, stderr)
	{
		fs.copySync("bower.json", "src/js/bower.json");

			// copies and rewrites the new bower files
		fs.copySync(".bowerrc", "src/js/.bowerrc");
		fs.writeFile("src/js/.bowerrc",
		 '{"directory": "components"}', 
		 function(err){
		 	if(err){return console.log(err);}
	
		 	console.log(".bowerrc file was saved!");
		 });

		remover('bower.json', errorLogger);
		remover('.bowerrc', errorLogger);

		startSubModules();
	};

	var child;

	

	var installPreloadJS = function(err, out, stderr)
	{
		var installProcess = child_process.exec('bower install PreloadJS --save', installSoundJS);
	};

	var installSoundJS = function(err, out, stderr)
	{
		var installProcess = child_process.exec('bower install SoundJS --save', installTweenJS);
	};

	var installTweenJS = function(err, out, stderr)
	{
		var installProcess = child_process.exec('bower install TweenJS --save', installEaselJS);
	};

	var installEaselJS = function(err, out, stderr)
	{
		var installProcess = child_process.exec('bower install easeljs --save', copyCurrentBower);
	};

	var installPixiJS = function(err, out, stderr)
	{
		var installProcess = child_process.exec('bower install pixi --save', installSoundJSForPixi);
	};

	var installSoundJSForPixi = function (err, out, stderr) {
		var installProcess = child_process.exec('bower install SoundJS --save', installPreloadJSForPixi);
	}

	var installPreloadJSForPixi = function(err, out, stderr)
	{
		var installProcess = child_process.exec('bower install PreloadJS --save', installParticlesForPixi);
	};

	var installParticlesForPixi = function(err, out, stderr) {
		var installProcess = child_process.exec('bower install pixi-particles --save', installTweenmaxForPixi);
	}

	var installTweenmaxForPixi = function (err, out, stderr) {
		var installProcess = child_process.exec('bower install gsap --save', copyCurrentBower);
	}

	var installBower = function(err, out, stderr)
	{
		var installProcess = child_process.exec('bower install', copyCurrentBower);
	};

	if(global.project_type == "p")
	{
		installPixiJS(0, 0, 0);
	}
	else if(global.project_type == "c")
	{
		installPreloadJS(0, 0, 0);
	}
	else
	{
		installBower(0, 0, 0);
	}


}

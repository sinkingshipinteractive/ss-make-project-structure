/* global require, console, __dirname */
"use strict";
var git = require("gulp-git");
var gulp = require("gulp");
var copyFile = require("fast-copy-file");
var copyDir = require("copy-dir");
var cssmin = require("gulp-cssmin");
var uglify = require("gulp-uglify");
var rename = require ("gulp-rename");
var jshint = require("gulp-jshint");
var svgParse = require("svg-to-bezier-path");
var compileConfig = require("ss-compile-config");

var tactical = require("tactical");
var stripDebug = require("strip-debug-blocks");

gulp.task("svg", function () {
	var svgPath = "./RawAssets/svg/";
	var outputPath = "./src/js/config/bezier/";
	svgParse(svgPath , outputPath);
})

gulp.task("config", function () {
	compileConfig("./src/config", "./src/js/config.json");
})


gulp.task("hint", function() {
	gulp.src([ "src/js/**/*.js", 
		"!src/js/weblib/external/**/*.js", "!src/js/components/**/*.js" ])
		.pipe(jshint())
		.pipe(jshint.reporter("default"));
});

gulp.task("default", function() {
	// compress & move css
	console.log("compressing & moving css");
	gulp.src("src/css/**/*.css")
		.pipe(cssmin())
	.pipe(gulp.dest("builds/release/css"));

	console.log("compiling config");
	compileConfig("./src/config", "./src/js/config.json");
	copyFile("src/js/config.json", "builds/release/js/config.json", function(err) {});
	

	// copy directories that we don't want to do anything to.
	console.log("copying directories");
	copyDir.sync(	"src/audio", 				"builds/release/audio");
	copyDir.sync(	"src/fonts", 				"builds/release/fonts");
	copyDir.sync(	"src/videos", 				"builds/release/videos");
	copyDir.sync(	"src/data",					"builds/release/data");
	copyDir.sync(	"src/images",				"builds/release/images");

	// copy the index file.
	console.log("copying index file");
	copyFile(		"src/index_compiled.html",	"builds/release/index.html", function(err) {}); // jshint ignore:line

	gulp.src([ "src/js/**/*.js", "!src/js/weblib/external/**/*.js", "!src/js/components/**/*.js"])
		.pipe(jshint())
		.pipe(jshint.reporter("default"));

	gulp.src("src/js/main.js")
		.pipe( tactical ("main.js", { "baseURL" : "src/js/", "console" : true }))
		.pipe(stripDebug())
		.pipe(uglify())
		.pipe(rename("compiled.js"))
	.pipe (gulp.dest("builds/release/js"));

});


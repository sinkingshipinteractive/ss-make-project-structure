/* global require, ss, $, console */
require.config ( {
	"baseUrl" :		"js",
	"alias": {"core" : [
                       "weblib/asset/CreateJSAssetManager",
                       "weblib/pools/PoolManager",
                       "weblib/command/CommandTree",
                       "weblib/command/CommandSet",
                       "local-namespace",
                       "weblib/sound/SoundPackage"
                       ]},
	"ignore": [],
	"include": [],
	"debug"   : 	true,
	"synced"  : 	true,
	"useFullPath": true
} );

(function () {
	"use strict";
	var $canvas = $( "canvas#gameCanvas" );

	require.ready(function()
	{

	});
}()
);
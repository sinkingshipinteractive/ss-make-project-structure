/* global require, ss, $, console */
require.config ( {
	"baseUrl" :		"js",
	"alias": {},
	"ignore": [],
	"include": [
		"components/jquery/dist/jquery.min.js",
		"components/pixi.js/dist/pixi.min.js",
		"components/PreloadJS/lib/preloadjs-0.6.2.min.js",
		"components/SoundJS/lib/soundjs-NEXT.min.js",
		"components/SoundJS/lib/flashaudioplugin-NEXT.min.js"
	],
	"debug"   : 	true,
	"synced"  : 	true,
	"useFullPath": true
} );


require.include("weblib-pixi/audio/AudioPackage");
require.include("weblib-pixi/utils/Util");
require.include("weblib-pixi/utils/SystemInfo");
require.include("weblib-pixi/math/MathPackage");
require.include("managers/GameManager");

(function () {
	"use strict";
	var $canvas = $( "canvas#gameCanvas" );
	var _gameManager;

	require.ready(function()
	{
		$.ajax({ dataType:"json",
			"url": "js/config.json",
			success: function (data) {
				if ( typeof ( window.ss ) === String ( undefined ) ) {
					window.ss = new Object(); // jshint ignore:line
				}// jshint ignore:line

				ss.config = data;
				startGame();
			}
		});
	});

	function startGame() {
		/*debug*/
		console.log("STARTING GAME WITH CONFIG:");
		console.dir(ss.config);
		/*/debug*/

		_gameManager = new ss.GameManager();
		_gameManager.startGame();

	}
}()
);
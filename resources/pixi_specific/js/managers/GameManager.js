/* global createjs, require, ss */
require.include("weblib-pixi/scene/SceneManager");
require.include("scenes/PreloadScene");

( function() {
	"use strict";

	function GameManager () {

		// locally scoped copy of "this".
		var _this = this;

		// Canvas to draw the game scene to.
		var _renderer;

		var _sceneManager;

		var _stage;

		function _construct () {

			_stage = new PIXI.Container();

			_renderer = PIXI.autoDetectRenderer(ss.config.global.gameWidth, ss.config.global.gameHeight);
		 	_renderer.view.style.position = "absolute";
			_renderer.view.style.display = "block";
			_renderer.autoResize = true;

			document.body.appendChild(_renderer.view);

			return _this;
		}

		/*
		* Start the game.
		*/
		_this.startGame = function() {
			_sceneManager = new ss.SceneManager();

			// TODO: Insert the 1st scene of your game in this list, and set it as the next scene of the preload scene.
			_sceneManager.addNewScene({sceneName: "PreloadScene", nextScene: undefined, prevScene : undefined, scene:ss.PreloadScene});

			_stage.addChild(_sceneManager);

			_sceneManager.gotoScene("PreloadScene");

			_startGameLoop();
		};

		var _lastTimeUpdate;

		/*
		* Start running the game loop.
		*/
		function _startGameLoop () {
			if ( !window.requestAnimationFrame ) {

				window.requestAnimationFrame = ( function() {

					return window.webkitRequestAnimationFrame ||
					window.mozRequestAnimationFrame ||
					window.oRequestAnimationFrame ||
					window.msRequestAnimationFrame ||
					function( /* function FrameRequestCallback */ callback, /* DOMElement Element */ element ) {

						window.setTimeout ( _handleGameTick, 1000 / 60 );

					};

				} )();

			}

			_lastTimeUpdate = new Date ().getTime ();
			requestAnimationFrame ( _handleGameTick );
		}

		function _handleGameTick () {
			var currentTime = new Date ().getTime ();

			var delta = ( currentTime - _lastTimeUpdate ) / 1000;

		    requestAnimationFrame ( _handleGameTick );

		    _lastTimeUpdate = currentTime;

		    _sceneManager.handleUpdate(delta);
		    _renderer.render(_stage);
		}

		return _construct();
	}

	ss.GameManager = GameManager;
}());


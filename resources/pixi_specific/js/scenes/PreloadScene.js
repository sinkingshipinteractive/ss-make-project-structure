/* global createjs, require, ss */
require.include("weblib-pixi/ssnamespace.js");
require.include("weblib-pixi/event/ObjectEventSystem.js");
require.include("weblib-pixi/scene/BaseScene");
require.include("weblib-pixi/utils/Spriteutils");

(function() {
	"use strict";


	function PreloadScene(sceneProperties, sceneData) {

		ss.BaseScene.call(this, sceneProperties, sceneData);

		var _this = this;

		var _loadedText;
		var _startButton;

		_this.startScene = function() {
			_loadedText = new PIXI.Text("0%", {fill:"white", align:"center"});
			_this.addChild(_loadedText);

			_loadedText.x = ss.config.global.gameWidth / 2;
			_loadedText.y = ss.config.global.gameHeight / 2;

			_startPreload();
		}

		function _startPreload() {

			PIXI.loader.onComplete.add(_onPreloadComplete);


			for ( var i = 0; i < ss.config.assets.preloadAssets.assets.length; i++) {

				PIXI.loader.add(ss.config.assets.preloadAssets.assets[i].id, ss.config.assets.preloadAssets.assets[i].src);
			}

			PIXI.loader.load();
		}

		function _loadProgress(evt) {
			_loadedText.text = PIXI.loader.progress + "%";
		}

		function _onPreloadComplete() {
			PIXI.loader.reset();
			PIXI.loader.onProgress.add(_loadProgress);
			PIXI.loader.onComplete.detachAll();
			PIXI.loader.onComplete.add(_complete);

			for ( var i = 0; i < ss.config.assets.globalAssets.assets.length; i++) {
				PIXI.loader.add(ss.config.assets.globalAssets.assets[i].id, ss.config.assets.globalAssets.assets[i].src);
			}

			PIXI.loader.load();
		}

		function _complete() {

			_this.removeChild(_loadedText);

			_startButton = ss.SpriteUtils.getBasicButton("playBtn", _startClicked);
			ss.SpriteUtils.centerInArea(_startButton, 0, 0, ss.config.global.gameWidth, ss.config.global.gameHeight);

			_this.addChild(_startButton);

		}

		function _startClicked() {
			_startButton.release(_startButton);
			_startButton.destroy();
			_this.nextScene();
		}

	}


	PreloadScene.prototype = new ss.BaseScene();
	PreloadScene.prototype.constructor = PreloadScene;

	ss.PreloadScene = PreloadScene;

}());

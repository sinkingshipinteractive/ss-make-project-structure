/* global require */


var readline = require("readline");

var rl = readline.createInterface({input: process.stdin,
		output: process.stdout});

// CREATE THE PROJECT FOLDERS.
	var createFolders = require("./lib/create_project_folders.js");

	// CREATE README.MD
	var createReadme = require("./lib/create_readme.js");

	//IMPORT PACKAGES && SUBMODULES
	var importPackages = require("./lib/import_packages.js");

	// SETUP GULPFILE.
	var copyResources = require("./lib/copy_resource_files.js");

	//CLEAN UP FOLDER
	var cleanup = require("./lib/cleanup.js");


rl.question("What type of project (p)ixi, (c)reatejs or (e)mpty: ", function(answer)
	{
		if(answer[0].toLowerCase() == "p")
		{
			global.project_type = "p";
			console.log("Pixi Project");
		}
		else if(answer[0].toLowerCase() == "c")
		{
			global.project_type = "c";
			console.log("CreateJS Project");
		}
		else
		{
			global.project_type = "e";
			console.log("Empty Proj");
		}

		rl.close();

		createFolders();
		createReadme();
		importPackages();
		copyResources();
		cleanup();
});

